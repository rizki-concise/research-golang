package researchgolang

import (
	"context"
	"fmt"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/logger"
)

func OpenConnection() *gorm.DB {
	dsn := "host=localhost user=postgres password=postgres dbname=research-golang port=5433 sslmode=disable TimeZone=Asia/Shanghai"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger:                 logger.Default.LogMode(logger.Info),
		SkipDefaultTransaction: true,
		PrepareStmt:            true,
	})

	if err != nil {
		panic(err)
	}

	sqlDB, err := db.DB()

	if err != nil {
		panic(err)
	}

	sqlDB.SetMaxOpenConns(100)
	sqlDB.SetMaxIdleConns(10)
	sqlDB.SetConnMaxLifetime(30 * time.Minute)
	sqlDB.SetConnMaxIdleTime(5 * time.Minute)

	return db
}

var db = OpenConnection()

func TestOpenConnection(t *testing.T) {
	assert.NotNil(t, db)
}

func TestExecuteSQL(t *testing.T) {
	err := db.Exec("INSERT INTO users(id, username, password, first_name) VALUES(?, ?, ?, ?)", "5a33388f-fd42-4de4-a701-034ed2809de1", "dsrizki", "qwer", "rizki").Error
	assert.Nil(t, err)

	err = db.Exec("INSERT INTO users(id, username, password, first_name) VALUES(?, ?, ?, ?)", "5a33388f-fd42-4de4-a701-034ed2809de2", "budi", "qwer", "budi").Error
	assert.Nil(t, err)

	err = db.Exec("INSERT INTO users(id, username, password, first_name) VALUES(?, ?, ?, ?)", "5a33388f-fd42-4de4-a701-034ed2809de3", "joko", "qwer", "joko").Error
	assert.Nil(t, err)

	err = db.Exec("INSERT INTO users(id, username, password, first_name) VALUES(?, ?, ?, ?)", "5a33388f-fd42-4de4-a701-034ed2809de4", "herman", "qwer", "herman").Error
	assert.Nil(t, err)
}

type Sample struct {
	Id       string
	Username string
}

func TestRawSQL(t *testing.T) {
	var sample Sample

	err := db.Raw("SELECT id, username FROM users WHERE id = ?", "5a33388f-fd42-4de4-a701-034ed2809de1").Scan(&sample).Error

	assert.Nil(t, err)
	assert.Equal(t, "dsrizki", sample.Username)

	var samples []Sample

	err = db.Raw("SELECT id, username FROM users").Scan(&samples).Error

	assert.Nil(t, err)
	assert.Equal(t, 4, len(samples))
}

func TestSqlRow(t *testing.T) {
	rows, err := db.Raw("SELECT id, username FROM users").Rows()

	assert.Nil(t, err)
	defer rows.Close()

	var samples []Sample
	for rows.Next() {
		var id string
		var username string

		err := rows.Scan(&id, &username)
		assert.Nil(t, err)

		samples = append(samples, Sample{
			Id:       id,
			Username: username,
		})
	}
	assert.Equal(t, 4, len(samples))
}

func TestScanRow(t *testing.T) {
	rows, err := db.Raw("SELECT id, username FROM users").Rows()

	assert.Nil(t, err)
	defer rows.Close()

	var samples []Sample

	for rows.Next() {
		err := db.ScanRows(rows, &samples)
		assert.Nil(t, err)
	}

	assert.Equal(t, 4, len(samples))
}

func TestCreateUser(t *testing.T) {
	user := User{
		ID:       "5a33388f-fd42-4de4-a701-034ed2809de5",
		Username: "dsrizkii",
		Password: "qwer",
		Name: Name{
			FirstName:  "Rizki",
			MiddleName: "D",
			LastName:   "S",
		},
		BirthDate:   time.Time{},
		Date:        DateStatus{},
		Information: "Ignored Column",
	}

	response := db.Create(&user)

	assert.Nil(t, response.Error)
	assert.Equal(t, int64(1), response.RowsAffected)
}

func TestBatchInsert(t *testing.T) {
	var users []User

	for i := 0; i < 10; i++ {
		users = append(users, User{
			ID:       "5a33388f-fd42-4de4-a701-034ed2809df" + strconv.Itoa(i),
			Username: "dsrizki" + strconv.Itoa(i),
			Email:    "mail" + strconv.Itoa(i) + "@mail.com",
			Password: "qwer",
			Name: Name{
				FirstName:  "Rizki ",
				MiddleName: "D",
				LastName:   "S",
			},
			PhoneNumber: strconv.Itoa(i),
			Information: "Ignored Column",
		})
	}

	result := db.Create(&users)

	assert.Nil(t, result.Error)
	assert.Equal(t, 10, int(result.RowsAffected))
}

func TestTransactionSuccess(t *testing.T) {
	err := db.Transaction(func(tx *gorm.DB) error {
		err := tx.Create(&User{
			ID:       "5a33388f-fd42-4de4-a701-034ed2809da0",
			Username: "testTx0",
			Email:    "emailtx0",
			Password: "qwer",
			Name: Name{
				FirstName: "Tx",
			},
			PhoneNumber: "08120",
		}).Error

		if err != nil {
			return err
		}

		err = tx.Create(&User{
			ID:       "5a33388f-fd42-4de4-a701-034ed2809da1",
			Username: "testTx1",
			Email:    "emailtx1",
			Password: "qwer",
			Name: Name{
				FirstName: "Tx",
			},
			PhoneNumber: "08121",
		}).Error

		if err != nil {
			return err
		}

		err = tx.Create(&User{
			ID:       "5a33388f-fd42-4de4-a701-034ed2809da2",
			Username: "testTx2",
			Email:    "emailtx2",
			Password: "qwer",
			Name: Name{
				FirstName: "Tx",
			},
			PhoneNumber: "08122",
		}).Error

		if err != nil {
			return err
		}

		return nil
	})

	assert.Nil(t, err)
}

func TestTransactionRollback(t *testing.T) {
	err := db.Transaction(func(tx *gorm.DB) error {
		err := tx.Create(&User{
			ID:       "5a33388f-fd42-4de4-a701-034ed2809da4",
			Username: "testTx4",
			Email:    "emailtx4",
			Password: "qwer",
			Name: Name{
				FirstName: "Tx",
			},
			PhoneNumber: "08124",
		}).Error

		if err != nil {
			return err
		}

		err = tx.Create(&User{
			ID:       "5a33388f-fd42-4de4-a701-034ed2809da0",
			Username: "testTx0",
			Email:    "emailtx0",
			Password: "qwer",
			Name: Name{
				FirstName: "Tx",
			},
			PhoneNumber: "08120",
		}).Error

		if err != nil {
			return err
		}

		return nil
	})

	assert.NotNil(t, err)
}

func TestManualTransactionSuccess(t *testing.T) {
	tx := db.Begin()
	defer tx.Rollback()

	err := tx.Create(&User{
		ID:       "5a33388f-fd42-4de4-a701-034ed2809da4",
		Username: "testTx4",
		Email:    "emailtx4",
		Password: "qwer",
		Name: Name{
			FirstName: "Tx",
		},
		PhoneNumber: "08124",
	}).Error

	assert.Nil(t, err)

	err = tx.Create(&User{
		ID:       "5a33388f-fd42-4de4-a701-034ed2809da5",
		Username: "testTx5",
		Email:    "emailtx5",
		Password: "qwer",
		Name: Name{
			FirstName: "Tx",
		},
		PhoneNumber: "08125",
	}).Error

	assert.Nil(t, err)

	if err == nil {
		tx.Commit()
	}
}

func TestManualTransactionRollback(t *testing.T) {
	tx := db.Begin()
	defer tx.Rollback()

	err := tx.Create(&User{
		ID:       "5a33388f-fd42-4de4-a701-034ed2809da6",
		Username: "testTx6",
		Email:    "emailtx6",
		Password: "qwer",
		Name: Name{
			FirstName: "Tx",
		},
		PhoneNumber: "08126",
	}).Error

	assert.Nil(t, err)

	err = tx.Create(&User{
		ID:       "5a33388f-fd42-4de4-a701-034ed2809da6",
		Username: "testTx6",
		Email:    "emailtx6",
		Password: "qwer",
		Name: Name{
			FirstName: "Tx",
		},
		PhoneNumber: "08126",
	}).Error

	assert.NotNil(t, err)

	if err == nil {
		tx.Commit()
	}
}

func TestQuerySingleObject(t *testing.T) {
	user := User{}

	err := db.First(&user).Error

	assert.Nil(t, err)
	assert.Equal(t, "5a33388f-fd42-4de4-a701-034ed2809da0", user.ID)

	user = User{}

	err = db.Last(&user).Error

	assert.Nil(t, err)
	assert.Equal(t, "5a33388f-fd42-4de4-a701-034ed2809df9", user.ID)
}

func TestQueryInlineCondition(t *testing.T) {
	user := User{}

	err := db.Take(&user, "id = ?", "5a33388f-fd42-4de4-a701-034ed2809df0").Error

	assert.Nil(t, err)
	assert.Equal(t, "dsrizki0", user.Username)
}

func TestQueryAllObjects(t *testing.T) {
	var users []User

	err := db.Find(&users, "id in ?", []string{
		"5a33388f-fd42-4de4-a701-034ed2809df0",
		"5a33388f-fd42-4de4-a701-034ed2809df1",
		"5a33388f-fd42-4de4-a701-034ed2809df2",
		"5a33388f-fd42-4de4-a701-034ed2809df3"}).Error

	assert.Nil(t, err)
	assert.Equal(t, 4, len(users))
}

func TestQueryCondition(t *testing.T) {
	var users []User

	err := db.Where("username LIKE ?", "%dsrizki%").Where("password = ?", "qwer").Find(&users).Error

	assert.Nil(t, err)
	assert.Equal(t, 12, len(users))
}

func TestOrOperator(t *testing.T) {
	var users []User

	err := db.Where("username LIKE ?", "%dsrizki%").Or("password = ?", "qwer").Find(&users).Error

	assert.Nil(t, err)
	assert.Equal(t, 20, len(users))
}

func TestNotOperator(t *testing.T) {
	var users []User

	err := db.Not("username LIKE ?", "%dsrizki%").Where("password = ?", "qwer").Find(&users).Error

	assert.Nil(t, err)
	assert.Equal(t, 8, len(users))
}

func TestSelectFields(t *testing.T) {
	var users []User

	err := db.Select("id, username").Find(&users).Error

	assert.Nil(t, err)
	assert.Equal(t, 20, len(users))

	for _, user := range users {
		assert.NotNil(t, user.ID)
		assert.NotEqual(t, "", user.Username)
	}
}

func TestStructCondition(t *testing.T) {
	userCondition := User{
		Name: Name{
			FirstName: "joko",
			LastName:  "", // tidak bisa dibuat condition
		},
		Password: "qwer",
	}

	var users []User

	err := db.Where(userCondition).Find(&users).Error

	assert.Nil(t, err)
	assert.Equal(t, 1, len(users))
}

func TestMapCondition(t *testing.T) {
	mapCondition := map[string]interface{}{
		"middle_name": "",
	}

	var users []User

	err := db.Where(mapCondition).Find(&users).Error

	assert.Nil(t, err)
	assert.Equal(t, 5, len(users))
}

func TestOrderLimitOffset(t *testing.T) {
	var users []User

	err := db.Order("id ASC, first_name DESC").Limit(5).Offset(5).Find(&users).Error

	assert.Nil(t, err)
	assert.Equal(t, 5, len(users))
}

type UserResponse struct {
	ID        string
	FirstName string
	LastName  string
}

func TestQueryNonModel(t *testing.T) {
	var users []UserResponse

	err := db.Model(&User{}).Select("id, first_name, last_name").Find(&users).Error

	assert.Nil(t, err)
	assert.Equal(t, 20, len(users))
	fmt.Println(users)
}

func TestUpdate(t *testing.T) {
	user := User{}

	err := db.Take(&user, "id = ?", "5a33388f-fd42-4de4-a701-034ed2809df0").Error

	assert.Nil(t, err)

	user.Username = "dsrizki_updated"
	user.Name.MiddleName = "_updated"
	user.Name.LastName = "lastname_updated"
	user.Password = "rahasia123"

	err = db.Save(&user).Error

	assert.Nil(t, err)
}

func TestSelectedColumns(t *testing.T) {
	err := db.Model(&User{}).Where("id = ?", "5a33388f-fd42-4de4-a701-034ed2809df0").Updates(map[string]interface{}{
		"middle_name": "",
		"last_name":   "cihuy",
	}).Error

	assert.Nil(t, err)

	err = db.Model(&User{}).Where("id = ?", "5a33388f-fd42-4de4-a701-034ed2809df0").Update("password", "diubahlagi").Error

	assert.Nil(t, err)

	err = db.Where("id = ?", "5a33388f-fd42-4de4-a701-034ed2809df0").Updates(User{
		Name: Name{
			FirstName: "Rizki",
			LastName:  "DS",
		},
	}).Error

	assert.Nil(t, err)
}

func TestAutoIncrement(t *testing.T) {
	for i := 0; i < 10; i++ {
		userLog := UserLog{
			UserId: "5a33388f-fd42-4de4-a701-034ed2809df0",
			Action: "Test Action",
		}

		err := db.Create(&userLog).Error

		assert.Nil(t, err)

		assert.NotEqual(t, 0, userLog.ID)
		fmt.Println(userLog.ID)
	}
}

func TestSaveOrUpdate(t *testing.T) {
	userLog := UserLog{
		UserId: "5a33388f-fd42-4de4-a701-034ed2809df0",
		Action: "Test Action",
	}

	err := db.Save(&userLog).Error

	assert.Nil(t, err)

	userLog.UserId = "5a33388f-fd42-4de4-a701-034ed2809df1"

	err = db.Save(&userLog).Error

	assert.Nil(t, err)
}

func TestSaveOrUpdateNonAutoIncrement(t *testing.T) {
	user := User{
		ID:       "5a33388f-fd42-4de4-a701-034ed280abc0",
		Username: "barunich",
		Name: Name{
			FirstName: "barunich",
		},
		Email:       "update/create from db.Save()",
		PhoneNumber: "update/create from db.Save()",
	}

	err := db.Save(&user).Error

	assert.Nil(t, err)

	user.Name.FirstName = "hello"

	err = db.Save(&user).Error

	assert.Nil(t, err)
}

func TestConflict(t *testing.T) {
	user := User{
		ID:       "5a33388f-fd42-4de4-a701-034ed280abc1",
		Username: "barunichhhh",
		Name: Name{
			FirstName: "barunichhhh",
		},
		Email:       "update/create from db.Clauses()",
		PhoneNumber: "update/create from db.Clauses()",
	}

	err := db.Clauses(clause.OnConflict{UpdateAll: true}).Create(&user).Error

	assert.Nil(t, err)
}

func TestDelete(t *testing.T) {
	var user User

	err := db.Take(&user, "id = ?", "5a33388f-fd42-4de4-a701-034ed2809df0").Error

	assert.Nil(t, err)

	err = db.Delete(&User{}, "id = ?", "5a33388f-fd42-4de4-a701-034ed2809df1").Error

	assert.Nil(t, err)

	err = db.Where("id = ?", "5a33388f-fd42-4de4-a701-034ed2809df2").Delete(&User{}).Error

	assert.Nil(t, err)
}

func TestSoftDelete(t *testing.T) {
	user := User{
		ID:       "5a33388f-fd42-4de4-a701-034ed280abc2",
		Username: "softdelete",
		Name: Name{
			FirstName: "softdelete",
		},
		Email:       "softdelete db.Clauses()",
		PhoneNumber: "softdelete db.Clauses()",
	}

	err := db.Create(&user).Error

	assert.Nil(t, err)

	err = db.Delete(&user).Error

	assert.Nil(t, err)
	assert.NotNil(t, user.Date.DeletedAt)

	var users []User

	err = db.Find(&users).Error

	assert.Nil(t, err)
}

func TestScoped(t *testing.T) {
	var user User

	result := db.Unscoped().First(&user, "id = ?", "5a33388f-fd42-4de4-a701-034ed280abc2").Error

	assert.Nil(t, result)

	result = db.Unscoped().Delete(&user).Error

	assert.Nil(t, result)

	var users []User

	err := db.Unscoped().Find(&users).Error

	assert.Nil(t, err)
}

func TestLock(t *testing.T) {
	err := db.Transaction(func(tx *gorm.DB) error {
		var user User

		err := tx.Clauses(clause.Locking{Strength: "UPDATE"}).Take(&user, "id = ?", "5a33388f-fd42-4de4-a701-034ed280abc1").Error

		if err != nil {
			return err
		}

		user.Username = "update from Lock"
		user.Name.FirstName = "rizki"
		user.Name.LastName = "ds"
		user.Email = "update from Lock"
		user.PhoneNumber = "update from Lock"

		err = tx.Save(&user).Error

		return err
	})

	assert.Nil(t, err)
}

func TestCreateWallet(t *testing.T) {
	wallet := Wallet{
		ID:      "4171b252-e4d7-4d00-ae2f-6764015f7739",
		UserId:  "5a33388f-fd42-4de4-a701-034ed2809de1",
		Balance: 1000000,
	}

	err := db.Create(&wallet).Error

	assert.Nil(t, err)
}

func TestRetrieveRelation(t *testing.T) {
	var user User

	err := db.Model(&User{}).Preload("Wallet").Take(&user, "id = ?", "5a33388f-fd42-4de4-a701-034ed2809de1").Error

	assert.Nil(t, err)
	assert.Equal(t, "5a33388f-fd42-4de4-a701-034ed2809de1", user.ID)
	assert.Equal(t, "4171b252-e4d7-4d00-ae2f-6764015f7739", user.Wallet.ID)
}

func TestRetrieveRelationJoin(t *testing.T) {
	var user User

	err := db.Model(&User{}).Joins("Wallet").Take(&user, "users.id = ?", "5a33388f-fd42-4de4-a701-034ed2809de1").Error

	assert.Nil(t, err)
	assert.Equal(t, "5a33388f-fd42-4de4-a701-034ed2809de1", user.ID)
	assert.Equal(t, "4171b252-e4d7-4d00-ae2f-6764015f7739", user.Wallet.ID)
}

func TestAutoCreateUpdate(t *testing.T) {
	user := User{
		ID:       "5a33388f-fd42-4de4-a701-034ed280abc3",
		Username: "autoCreateUpdate",
		Password: "qwer",
		Name: Name{
			FirstName: "autoCreateUpdate",
		},
		Email:       "autoCreateUpdate",
		PhoneNumber: "autoCreateUpdate",
		Wallet: Wallet{
			ID:      "5a33388f-fd42-4de4-a701-034ed280abc4",
			UserId:  "5a33388f-fd42-4de4-a701-034ed280abc3",
			Balance: 500000,
		},
	}

	err := db.Create(&user).Error

	assert.Nil(t, err)
}

func TestSkipCreateUpdate(t *testing.T) {
	user := User{
		ID:       "5a33388f-fd42-4de4-a701-034ed280abc4",
		Username: "autoCreateUpdate2",
		Password: "qwer2",
		Name: Name{
			FirstName: "autoCreateUpdate2",
		},
		Email:       "autoCreateUpdate2",
		PhoneNumber: "autoCreateUpdate2",
		Wallet: Wallet{
			ID:      "5a33388f-fd42-4de4-a701-034ed280abc5",
			UserId:  "5a33388f-fd42-4de4-a701-034ed280abc4",
			Balance: 600000,
		},
	}

	err := db.Omit(clause.Associations).Create(&user).Error

	assert.Nil(t, err)
}

func TestUserAndAddresses(t *testing.T) {
	user := User{
		ID:       "5a33388f-fd42-4de4-a701-034ed280abc5",
		Username: "TestUserAndAddresses",
		Password: "qwer",
		Name: Name{
			FirstName: "TestUserAndAddresses",
		},
		Email:       "TestUserAndAddresses",
		PhoneNumber: "TestUserAndAddresses",
		Wallet: Wallet{
			ID:      "5a33388f-fd42-4de4-a701-034ed280abc6",
			UserId:  "5a33388f-fd42-4de4-a701-034ed280abc5",
			Balance: 500000,
		},
		Addresses: []Address{
			{
				ID:      "9ba0012e-0d83-4982-a601-12aa746589f0",
				UserId:  "5a33388f-fd42-4de4-a701-034ed280abc5",
				Address: "Jalan A",
			},
			{
				ID:      "9ba0012e-0d83-4982-a601-12aa746589f1",
				UserId:  "5a33388f-fd42-4de4-a701-034ed280abc5",
				Address: "Jalan B",
			},
		},
	}

	err := db.Create(&user).Error

	assert.Nil(t, err)
}

func TestPreloadJoinOneToMany(t *testing.T) {
	var users []User

	err := db.Model(&User{}).Preload("Addresses").Joins("Wallet").Find(&users).Error

	assert.Nil(t, err)
}

func TestTakePreloadJoinOneToMany(t *testing.T) {
	var user User

	err := db.Model(&User{}).Preload("Addresses").Joins("Wallet").Take(&user, "users.id = ?", "5a33388f-fd42-4de4-a701-034ed280abc5").Error

	assert.Nil(t, err)
}

func TestBelongsToAddress(t *testing.T) {
	fmt.Println("Preload")

	var addresses []Address

	err := db.Model(&Address{}).Preload("User").Find(&addresses).Error

	assert.Nil(t, err)

	fmt.Println("Joins")

	addresses = []Address{}

	err = db.Model(&Address{}).Joins("User").Find(&addresses).Error

	assert.Nil(t, err)
}

func TestBelongsToWallet(t *testing.T) {
	fmt.Println("Preload")

	var wallets []Wallet

	err := db.Model(&Wallet{}).Preload("User").Find(&wallets).Error

	assert.Nil(t, err)

	fmt.Println("Joins")

	wallets = []Wallet{}

	err = db.Model(&Wallet{}).Joins("User").Find(&wallets).Error

	assert.Nil(t, err)
}

func TestCreateManyToMany(t *testing.T) {
	product := Product{
		ID:    "34e8a3e3-5cfa-4012-a8f8-2b9561471a7d",
		Name:  "Product 1",
		Price: 1000000,
	}

	err := db.Create(&product).Error

	assert.Nil(t, err)

	err = db.Table("user_like_product").Create(map[string]interface{}{
		"user_id":    "5a33388f-fd42-4de4-a701-034ed280abc5",
		"product_id": "34e8a3e3-5cfa-4012-a8f8-2b9561471a7d",
	}).Error

	assert.Nil(t, err)
}

func TestPreloadManyToMany(t *testing.T) {
	var product Product

	err := db.Preload("LikedByUsers").Take(&product, "id = ?", "34e8a3e3-5cfa-4012-a8f8-2b9561471a7d").Error

	assert.Nil(t, err)
	assert.Equal(t, 1, len(product.LikedByUsers))
}

func TestPreloadManyToManyUser(t *testing.T) {
	var user User

	err := db.Preload("LikeProducts").Take(&user, "id = ?", "5a33388f-fd42-4de4-a701-034ed280abc5").Error

	assert.Nil(t, err)
	assert.Equal(t, 1, len(user.LikeProducts))
}

func TestAssociationFind(t *testing.T) {
	var product Product

	err := db.Take(&product, "id = ?", "34e8a3e3-5cfa-4012-a8f8-2b9561471a7d").Error

	assert.Nil(t, err)

	var users []User

	err = db.Model(&product).Where("first_name ILIKE ?", "Test%").Association("LikedByUsers").Find(&users)

	assert.Nil(t, err)
	assert.Equal(t, 1, len(users))

	err = db.Model(&product).Where("users.first_name ILIKE ?", "Test%").Association("LikedByUsers").Find(&users)

	assert.Nil(t, err)
	assert.Equal(t, 1, len(users))
}

func TestAssociationAppend(t *testing.T) {
	var user User

	err := db.Take(&user, "id = ?", "5a33388f-fd42-4de4-a701-034ed280abc5").Error

	assert.Nil(t, err)

	var product Product

	err = db.Take(&product, "id = ?", "34e8a3e3-5cfa-4012-a8f8-2b9561471a7d").Error

	assert.Nil(t, err)

	db.Model(&product).Association("LikedByUsers").Append(&user)

	assert.Nil(t, err)
}

func TestAssociationReplace(t *testing.T) {
	err := db.Transaction(func(tx *gorm.DB) error {
		var user User

		err := tx.Take(&user, "id = ?", "5a33388f-fd42-4de4-a701-034ed280abc5").Error

		assert.Nil(t, err)

		wallet := Wallet{
			ID:      "887c87ca-dfaf-4c26-b3cb-3d2f5fdd1f1a",
			UserId:  user.ID,
			Balance: 550000,
		}

		tx.Model(&user).Association("Wallet").Replace(&wallet)

		return err
	})

	assert.NotNil(t, err)
}

func TestAssociationDelete(t *testing.T) {
	var user User

	err := db.Take(&user, "id = ?", "5a33388f-fd42-4de4-a701-034ed280abc5").Error

	assert.Nil(t, err)

	var product Product

	err = db.Take(&product, "id = ?", "34e8a3e3-5cfa-4012-a8f8-2b9561471a7d").Error

	assert.Nil(t, err)

	db.Model(&product).Association("LikedByUsers").Delete(&user)

	assert.Nil(t, err)
}

func TestAssociationClear(t *testing.T) {
	var product Product

	err := db.Take(&product, "id = ?", "34e8a3e3-5cfa-4012-a8f8-2b9561471a7d").Error

	assert.Nil(t, err)

	db.Model(&product).Association("LikedByUsers").Clear()

	assert.Nil(t, err)
}

func TestPreloadingWithCondition(t *testing.T) {
	var user User

	err := db.Preload("Wallet", "balance > ?", "500000").Take(&user, "id = ?", "5a33388f-fd42-4de4-a701-034ed280abc5").Error

	assert.Nil(t, err)
}

func TestNestedPreloading(t *testing.T) {
	var wallet Wallet

	err := db.Preload("User.Addresses").Take(&wallet, "id = ?", "5a33388f-fd42-4de4-a701-034ed280abc6").Error

	assert.Nil(t, err)
}

func TestPreloadAll(t *testing.T) {
	var user User

	err := db.Preload(clause.Associations).First(&user, "id = ?", "5a33388f-fd42-4de4-a701-034ed280abc5").Error

	assert.Nil(t, err)
}

func TestJoinQuery(t *testing.T) {
	var users []User

	err := db.Joins("JOIN wallets ON wallets.user_id = users.id").Find(&users).Error

	assert.Nil(t, err)
	assert.Equal(t, 3, len(users))

	users = []User{}

	err = db.Joins("Wallet").Find(&users).Error

	assert.Nil(t, err)
	assert.Equal(t, 23, len(users))
}

func TestJoinWithCondition(t *testing.T) {
	var users []User

	err := db.Joins("JOIN wallets ON wallets.user_id = users.id AND wallets.balance > ?", 500000).Find(&users).Error

	assert.Nil(t, err)
	assert.Equal(t, 1, len(users))

	users = []User{}

	err = db.Joins("Wallet").Where(`"Wallet"."balance" > ?`, 500000).Find(&users).Error

	assert.Nil(t, err)
	assert.Equal(t, 1, len(users))
}

func TestCount(t *testing.T) {
	var count int64

	err := db.Model(&User{}).Joins("Wallet").Where(`"Wallet"."balance" > ?`, 500000).Count(&count).Error

	assert.Nil(t, err)
	assert.Equal(t, int64(1), count)

}

type AggregationResult struct {
	TotalBalance int64
	MinBalance   int64
	MaxBalance   int64
	AvgBalance   float64
}

func TestAggregationResult(t *testing.T) {
	var result AggregationResult

	err := db.Model(&Wallet{}).Select("SUM(balance) AS total_balance", "MIN(balance) AS min_balance", "MAX(balance) AS max_balance", "AVG(balance) AS avg_balance").Take(&result).Error

	assert.Nil(t, err)
	assert.Equal(t, int64(2000000), result.TotalBalance)
	assert.Equal(t, int64(500000), result.MinBalance)
	assert.Equal(t, int64(1000000), result.MaxBalance)
	assert.Equal(t, float64(666666.6666666666), result.AvgBalance)
}

func TestAggregationGroupByAndHaving(t *testing.T) {
	var results []AggregationResult

	err := db.Model(&Wallet{}).Select("SUM(balance) AS total_balance", "MIN(balance) AS min_balance", "MAX(balance) AS max_balance", "AVG(balance) AS avg_balance").
		Joins("User").Group("User.id").Having("SUM(balance) >= ?", 500000).Find(&results).Error

	assert.Nil(t, err)
	assert.Equal(t, 3, len(results))
}

func TestContext(t *testing.T) {
	ctx := context.Background()

	var users []User

	err := db.WithContext(ctx).Find(&users).Error

	assert.Nil(t, err)
	assert.Equal(t, 23, len(users))
}

func BrokeWalletBalance(db *gorm.DB) *gorm.DB {
	return db.Where("balance = ?", 0)
}

func SultanWalletBalance(db *gorm.DB) *gorm.DB {
	return db.Where("balance >= ?", 1000000)
}

func TestScopes(t *testing.T) {
	var wallets []Wallet

	err := db.Scopes(BrokeWalletBalance).Find(&wallets).Error

	assert.Nil(t, err)

	err = db.Scopes(SultanWalletBalance).Find(&wallets).Error

	assert.Nil(t, err)

	err = db.Scopes(BrokeWalletBalance, SultanWalletBalance).Find(&wallets).Error

	assert.Nil(t, err)
}

func TestMigrator(t *testing.T) {
	err := db.Migrator().AutoMigrate(&GuestBook{})

	assert.Nil(t, err)
}

// TODO Hooks Create
// begin transaction
// * BeforeSave()
// * BeforeCreate()
// save before associations
// insert into database
// save after associations
// * AfterCreate()
// * AfterSave()
// commit or rollback transaction

// TODO Hooks Update
// begin transaction
// * BeforeSave()
// * BeforeUpdate()
// save before associations
// update into database
// save after associations
// * AfterUpdate()
// * AfterSave()
// commit or rollback transaction

// TODO Hooks Delete
// begin transaction
// * BeforeDelete()
// delete into database
// * AfterDelete()
// commit or rollback transaction

// TODO Hooks Find
// load data from database
// Preloading (eager loading)
// * AfterFind()

func TestHooks(t *testing.T) {
	user := User{
		Username: "hooks",
		Password: "qwer",
		Name: Name{
			FirstName: "hooks",
		},
		Email:       "hooks",
		PhoneNumber: "hooks",
	}

	err := db.Create(&user).Error

	assert.Nil(t, err)
	assert.NotEqual(t, "", user.ID)

	fmt.Println(user)
}
