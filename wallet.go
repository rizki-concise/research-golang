package researchgolang

type Wallet struct {
	ID      string     `gorm:"primary_key;column:id;<-:create"`
	UserId  string     `gorm:"column:user_id"`
	Balance int64      `gorm:"column:balance"`
	Date    DateStatus `gorm:"embedded"`
	User    *User      `gorm:"foreign_key:user_id;references:id"`
}

func (w Wallet) TableName() string {
	return "wallets"
}
