package researchgolang

type Address struct {
	ID      string     `gorm:"primary_key;column:id;<-:create"`
	UserId  string     `gorm:"column:user_id"`
	Address string     `gorm:"column:address"`
	Date    DateStatus `gorm:"embedded"`
	User    User       `gorm:"foreign_key:user_id;references:id"`
}

func (a *Address) TableName() string {
	return "addresses"
}
