CREATE TABLE wallets(
  id UUID PRIMARY KEY,
  user_id UUID NOT NULL REFERENCES users(id),
  balance BIGINT NOT NULL,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  deleted_at TIMESTAMP
);