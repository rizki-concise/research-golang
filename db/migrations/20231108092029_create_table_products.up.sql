CREATE TABLE products(
  id UUID PRIMARY KEY,
  name VARCHAR(50),
  price BIGINT NOT NULL,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  deleted_at TIMESTAMP
);