CREATE TABLE addresses(
  id UUID PRIMARY KEY,
  user_id UUID NOT NULL REFERENCES users(id),
  address TEXT NOT NULL,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  deleted_at TIMESTAMP
);