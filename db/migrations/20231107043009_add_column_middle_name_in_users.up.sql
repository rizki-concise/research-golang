ALTER TABLE users
  ADD COLUMN middle VARCHAR(50);

ALTER TABLE users
  RENAME COLUMN middle to middle_name;