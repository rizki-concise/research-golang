package researchgolang

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type User struct {
	ID           string     `gorm:"primary_key;column:id;<-:create"`
	Username     string     `gorm:"column:username"`
	Email        string     `gorm:"column:email"`
	Password     string     `gorm:"column:password"`
	Name         Name       `gorm:"embedded"`
	PhoneNumber  string     `gorm:"column:phone_number"`
	BirthDate    time.Time  `gorm:"column:birth_date"`
	Date         DateStatus `gorm:"embedded"`
	Information  string     `gorm:"-"`
	Wallet       Wallet     `gorm:"foreign_key:user_id;references:id"`
	Addresses    []Address  `gorm:"foreign_key:user_id;references:id"`
	LikeProducts []Product  `gorm:"many2many:user_like_product;foreignKey:id;joinForeignKey:user_id;references:id;joinReferences:product_id"`
}

func (u *User) TableName() string {
	return "users"
}

func (u *User) BeforeCreate(db *gorm.DB) error {
	if u.ID == "" {
		u.ID = uuid.NewString()
	}

	return nil
}

type Name struct {
	FirstName  string `gorm:"column:first_name"`
	MiddleName string `gorm:"column:middle_name"`
	LastName   string `gorm:"column:last_name"`
}

type DateStatus struct {
	CreatedtAt time.Time      `gorm:"column:created_at;autoCreateTime;<-:create"`
	UpdatedAt  time.Time      `gorm:"column:updated_at;autoCreateTime;autoUpdateTime"`
	DeletedAt  gorm.DeletedAt `gorm:"column:deleted_at;<-:update"`
}

type UserLog struct {
	ID     string     `gorm:"primary_key;column:id;autoIncrement;<-:create"`
	UserId string     `gorm:"column:user_id;<-:create"`
	Action string     `gorm:"column:action;<-:create"`
	Date   DateStatus `gorm:"embedded"`
}

func (l *UserLog) TableName() string {
	return "user_logs"
}
