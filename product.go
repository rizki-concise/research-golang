package researchgolang

type Product struct {
	ID           string     `gorm:"primary_key;column:id;<-:create"`
	Name         string     `gorm:"column:name"`
	Price        int64      `gorm:"column:price"`
	Date         DateStatus `gorm:"embedded"`
	LikedByUsers []User     `gorm:"many2many:user_like_product;foreignKey:id;joinForeignKey:product_id;references:id;joinReferences:user_id"`
}

func (p *Product) TableName() string {
	return "products"
}
