package researchgolang

type GuestBook struct {
	ID      string     `gorm:"primary_key;column:id;<-:create"`
	Name    string     `gorm:"column:name"`
	Email   string     `gorm:"column:email"`
	Message string     `gorm:"column:message"`
	Date    DateStatus `gorm:"embedded"`
}

func (g GuestBook) TableName() string {
	return "guest_books"
}
