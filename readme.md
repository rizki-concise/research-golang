1. Create new postgre DB Test manual

2. Migrate DB with this golang-migrate
```migrate -database "postgres://<user>:<password>@<host>:<port>/<db_name>?sslmode=disable" -path db/migrations up```

3. Run the test
```go test -run ''```

4. If you want re-run the test, clean the data first